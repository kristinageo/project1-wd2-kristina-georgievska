<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" type = "text/css" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel='stylesheet'>
    <title>BrainsterLabs - FullStack Academy</title>
    <style>
    
    </style>
  </head>
  <body>
  
    <!--NAVBAR BEGIN-->
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="navbar.html" >
          <div class="img" id="img">
              <img src="Logo.png" class="mt-4 image-margin" alt=""> 
              <figcaption class="text-uppercase font-weight-bold image-margin mt-1 text-center text-size">brainster</figcaption>
          </div>
        </a>
        <button class="navbar-toggler position d-lg-none" id="addCross" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>       
        </button>
        <button  style="display: none; border:none;" class="navbar-toggler position"  id="addAdd"  type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
          <i class='fa fa-times fa-2x'></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
          <ul class="navbar-nav m-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link font-weight-bold size-13" href="https://marketpreneurs.brainster.co">Академија за маркетинг <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link font-weight-bold size-13" href="https://codepreneurs.brainster.co">Академија за програмирање <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link font-weight-bold size-13" href="https://datascience.brainster.co/">Академија за data science <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link font-weight-bold size-13" href="https://design.brainster.co">Академија за дизајн <span class="sr-only">(current)</span></a>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">        
            <button class="btn btn-danger my-2 my-sm-0 size-13"  type="button" data-toggle="modal" data-target="#vrabotiStudent">Вработи наш студент</button>
          </form>
        </div>
        <div class="modal" id="vrabotiStudent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bg-yellow m-4">
              <div class="modal-header">
                <h1 class="modal-title" id="exampleModalLabel">Вработи студенти</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
              <form  method="post" action="navbar.php" id="submit-form">
                <div class="container col-lg-12">
                  <div class="row row-width">
                    <div class="col-md-6">
                            <div class="form-group">
                              <label for="name_surname" class="col-form-label font-weight-bold">Име и презиме</label>
                            <input type="text" class="form-control text-muted size-13 font-italic name_surname"  name="name_surname" id="name_surname" value="Вашето име и презиме">
                            <span class="red size-10" id="error-name-surname"></span>
                            </div>
                    </div>
                    <div class="col-md-6">
                          <div class="form-group">
                            <label for="name_company" class="col-form-label font-weight-bold">Име на компанија</label>
                          <input type="text" class="form-control text-muted size-13 font-italic" name="name_company" id="name_company" value="Име на вашата компанија">
                          <span class="red size-10" id="error-name-company"></span>
                          </div> 
                    </div>
                  </div>
                  <div class="row row-width">
                    <div class="col-md-6">
                            <div class="form-group">
                              <label for="email" class="col-form-label font-weight-bold">Контакт имејл</label>
                            <input type="text" class="form-control text-muted size-12 font-italic" name="email" id="email" value="Компаниски имејл на вашата компанија">
                            <span class="red size-10" id="error-email"></span>
                            </div>
                    </div>
                    <div class="col-md-6">
                          <div class="form-group">
                            <label for="mobile_phone" class="col-form-label font-weight-bold">Контакт телефон</label>
                          <input type="text" class="form-control text-muted size-12 font-italic" name="mobile_phone" id="mobile_phone" value="Контакт телефон на вашата компанија">
                          <span class="red size-10" id="error-mobile_phone"></span>
                          </div>  
                    </div>
                  </div>
                  <div class="row row-width">
                    <div class="col-md-6">
                            <div class="form-group">
                              <label for="recipient-name" class="col-form-label font-weight-bold">Тип на студенти</label>
                              <select id="type_of_student_id" name="type_of_student_id">
                              <option value="Изберете тип на студент" selected disabled hidden  class="font-weight-bold">Изберете тип на студент</option>
                              <?php  
                              session_start();  
                              $connect = mysqli_connect("127.0.0.1", "root", "", "project1"); 
                                $query = "SELECT * FROM type_of_student";  
                                $result = mysqli_query($connect, $query);  
                                if(mysqli_num_rows($result) > 0)  
                                {  
                                    while($row = mysqli_fetch_array($result))  //fetching from database type_of_student
                                    {  
                               ?> 
                                <option value="<?php echo $row["id"]; ?>" class="font-weight-bold"><?php echo $row["type_of_student"];?></option>
                                <?php
                                    }}
                               ?>
                              </select>
                            </div>
                            <span class="red size-10" id="error-students"></span>                     
                    </div>                
                    <div class="col-md-6">                  
                          <div class="form-group">
                             <button name="submit" type="submit" class="btn btn-danger btn-block mt-4 btn-click" >Испрати</button>
                          </div>             
                    </div>               
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <!--NAVBAR ENDS HERE-->
      <!--jumbotron-->
  <div class="main">
      <div class="jumbotron jumbotron-fluid d-none d-sm-block bg-cover">
        <div class="container">
           <div class="row">
             <div class="col-md-12">
             <h3 class="display-4 text-white text-center font-weight-bold d-flex justify-content-center align-self-center">Brainster Labs</h3>  
             </div>
           </div>
        
        </div>
    </div>
    <!--jum botron-->
    <div class="container-fluid selectSection">
              <div class="row" id="myBtnContainer">
                  <div class="col-lg-4 p-0">
                      <button class="btn active-btn  filter-button btn-main text-justify border-right" data-filter="marketing" data-number="1"> Проекти на студенти во академијата
                        <span class="content" data-number="1">
                          <i  class='bx bxs-check-circle fa-2x  pt-1 pr-3'></i>
                        </span>
                          <br/>  за маркетинг            
                      </button>
                  </div>
                  <div class="col-lg-4 p-0">                
                    <button class="btn  filter-button btn-main text-justify border-right" data-filter="programming" data-number="2"> Проекти на студенти во академијата
                      <span class="content" data-number="2">
                        <i  class='bx bxs-check-circle fa-2x  pt-1 pr-3'></i>
                      </span>
                      <br>за програмирање 
                    </button>
                  </div>
                  <div class="col-lg-4 p-0">
                      <button class="btn  filter-button btn-main text-justify" data-filter="design" data-number="3"> Проекти на студенти во академијата 
                        <span class="content" data-number="3">
                          <i  class='bx bxs-check-circle fa-2x  pt-1 pr-3 '></i>
                        </span>
                        <br>за дизајн              
                      </button>
                  </div>
              </div>    
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-md-center font-weight-bold pt-4 text-sm-left">
            <h2>Проекти</h2>
          </div>
        </div>
      </div>
      <div class="container">
          <div class="row offset-md-1">
      <?php
          for($m = 11; $m<=14;$m++){
      ?>     
                <div class="col-lg-3 col-md-6 col-sm-12 pCard  rounded design filter bg-white noPadding">
                <img src="../project1-wd2-kristina-georgievska/carts-images/design/cart<?=  $m; ?>.jpg" class="img pImg ImgClass rounded-top"/>
                <div class="p-2">
                  <p class="text-center size-13 bg-yellow p-1 w-75">Академија за дизајн</p>
                  <h5 class="text-left font-weight-bold">Име на проектот стои овде <br/> во две линии</h5>
                  <p class="text-left size-13">Краток опис во кој студентите ќе можат да опишат за што се работи во проектот</p>
                  <p class="text-left"><small class="font-weight-bold size-10">Април - Октомври 2019</small></p>
                  <a href="#" class="btn btn-danger float-right mb-2">Дознај повеќе</a>
                </div>
              </div> 
         <?php   
         }

     ?>
      <?php
          for($m = 1; $m<=10;$m++){
      ?>     
                <div class="col-lg-3 col-md-6 col-sm-12 pCard  rounded programming filter bg-white noPadding">
                <img src="../project1-wd2-kristina-georgievska/carts-images/programming/cart<?=  $m; ?>.jpg" class="img pImg ImgClass rounded-top"/>
                <div class="p-2">
                  <p class="text-center size-13 bg-yellow p-1 w-75">Академија за програмирање</p>
                  <h5 class="text-left font-weight-bold">Име на проектот стои овде <br/> во две линии</h5>
                  <p class="text-left size-13">Краток опис во кој студентите ќе можат да опишат за што се работи во проектот</p>
                  <p class="text-left"><small class="font-weight-bold size-10">Април - Октомври 2019</small></p>
                  <a href="#" class="btn btn-danger float-right mb-2">Дознај повеќе</a>
                </div>
              </div> 
         <?php   
         }

     ?>

      <?php
          for($m = 15; $m<=20;$m++){
      ?>     
                <div class="col-lg-3 col-md-6 col-sm-12 pCard rounded marketing filter bg-white noPadding">
                <img src="../project1-wd2-kristina-georgievska/carts-images/marketing/cart<?=  $m; ?>.jpg" class="img pImg ImgClass rounded-top"/>
                <div class="p-2">
                  <p class="text-center size-13 bg-yellow p-1 w-75">Академија за маркетинг</p>
                  <h5 class="text-left font-weight-bold">Име на проектот стои овде <br/> во две линии</h5>
                  <p class="text-left size-13">Краток опис во кој студентите ќе можат да опишат за што се работи во проектот</p>
                  <p class="text-left"><small class="font-weight-bold size-10">Април - Октомври 2019</small></p>
                  <a href="#" class="btn btn-danger float-right mb-2">Дознај повеќе</a>
                </div>
              </div> 
         <?php   
         }

     ?>
          </div>
      </div>
      <div class="container-fluid">
        <div class="row bg-dark">
            <div class="col-lg-12">
                 <p class="text-center text-white mt-2 color">Изработено со <span class="heart">&#9829;</span> од студентите на Brainster</p>
            </div>
        </div>
    </div>
 </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
 
    <script>
          $(document).ready(function(){
            $(".filter-button").click(function(){
              var value = $(this).attr('data-filter');
              if(value == "all"){
                $('.filter').show('1000');
              }else{
                $(".filter").not('.'+value).hide('3000');
                $('.filter').filter('.'+value).show('3000');
              }
             });
          });
      </script>
      <script>         
          let Buttons = document.querySelectorAll(".selectSection button");
          for (let button of Buttons) {
          button.addEventListener('click', (e) => { 
            const et = e.target;  
            const active = document.querySelector(".active-btn");   
            if (active) {
              active.classList.remove("active-btn");
            }
          et.classList.add("active-btn"); 
          let allContent = document.querySelectorAll('.content');
          for (let content of allContent) {        
              if(content.getAttribute('data-number') === button.getAttribute('data-number')) {
                content.style.display = "block";
              }         
              else {
                content.style.display = "none";
              }
            }
          });
        }
      </script> 


         <script>
            $(document).ready(function(){
                $("#addCross").click(function(){
                      document.getElementById("addCross").style.display="none";
                      document.getElementById("addAdd").style.display="inline-block";
                      document.getElementById("img").style.visibility = "none";
                     
                      document.body.style.backgroundColor="#302F38";
                      $(".nav-link").addClass("colorMe")
                      $("#img").css("display", "none");
                      // $("nav").css("width", "100vh");
                      // $("#addAdd").css("display", "block");
                      // $("#addAdd").css("display", "block");
                      $(".main").css("display", "none");
                });
                $("#addAdd").click(function(){
                      document.getElementById("addCross").style.display="inline-block";
                      document.getElementById("addAdd").style.display="none";
                      document.getElementById("img").style.visibility = "inline-block";
                      document.body.style.backgroundColor="#FCD232";
                      $(".nav-link").removeClass("colorMe");
                      $("#img").css("display", "block");
                      $(".main").css("display", "block");
                });         
            })   
        </script>
        <script>
          $(document).ready(function(){
            $("#submit-form").on("submit",function(event){
              event.preventDefault();
            if($('#name_surname').val()=="Вашето име и презиме" || $('#name_surname').val()==" "){
              $('#error-name-surname').html("* Name and surname is required");
              $('#error-name-surname').css("color","red");
            }
            if($('#name_company').val()=="Име на вашата компанија" ||  $('#name_company').val()==" " ){           
              $('#error-name-company').html("* Name of company is required");
              $('#error-name-company').css("color","red");
            }
            if($('#email').val()=="Компаниски имејл на вашата компанија" ||  $('#email').val()==" " ){
            
              $('#error-email').html("* Email is required");
              $('#error-email').css("color","red");
              }
              if($('#mobile_phone').val()=="Контакт телефон на вашата компанија" ||  $('#mobile_phone').val()==" " ){   
              $('#error-mobile_phone').html("* Contact phone is required");
              $('#error-mobile_phone').css("color","red");
              }
              if($('#type_of_student_id option:selected').text()=='Изберете тип на студент' || s > 3 || s < 0){          
              $('#error-students').html("* Type_of student is required");
              $('#error-students').css("color","red");
              }
              else{
                  var form = {
                  'name_surname': $('#name_surname').val(),
                  'name_company': $('$name_company').val(),
                  'email':$('#email').val(),
                  'mobile_phone': $('#mobile_phone').val(),
                  'type_of_student_id' : $('#type_of_student_id option:selected').val(),
                };
                  $.ajax({
                  url:'navbar.php',
                  type:'POST',
                  data: form,
                  dataType:'json',
                  success:function(data){
                  console.log('success');
                  },
                  error:function(data){
                      console.log('error');
                  }
                });
              }
            })   
          });
        </script>  
          <?php
          if(isset($_POST['name_surname'])){
            $name_surname = $_POST['name_surname'];
          }else{
            return;
          }
          if(isset($_POST['name_company'])){
            $name_company = $_POST['name_company'];
          }else{
            return;
          }
          if(isset($_POST['email'])){  
          $email = $_POST['email'];
          }else{
            return;
          }
          if(isset($_POST['mobile_phone'])){
            $mobile_phone= $_POST['mobile_phone'];
          }else{
            return;
          }
          if(isset($_POST['type_of_student_id'])){
            $type_of_student_id = $_POST['type_of_student_id'];
          }else{
            return;
          }
          if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
            // echo "Error email format <br/>";
            exit();
          }
          $sql = "INSERT INTO vraboti_student(name_surname, name_company,email,mobile_phone,type_of_student_id)
          VALUES ('".$_POST["name_surname"]."','".$_POST["name_company"]."','".$_POST["email"]."',
          '".$_POST["mobile_phone"]."',$type_of_student_id)";
          if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
            exit();
          }
          if (mysqli_query($connect, $sql)) {
            // echo "New record created successfully"; 
          } else {
            // echo "Error: " . $sql . "<br>";
          }
          mysqli_close($connect); 
          ?>  
     
 
</body>
</html>